var CROSS = 'X';
var ZERO = 'O';
var EMPTY = ' ';
let playerCounter = 0;
let boardSize = Number(prompt('Введите размер поля', 3));
let board = null;
let isGameOver = false;
startGame();

function startGame() {
    renderGrid(boardSize);
    board = createBoard();
    isGameOver = false;
    playerCounter = 0;
    showMessage('');
}
function createBoard() {
    let temp = Array.from(Array(boardSize), () => new Array(boardSize));
    for (let i = 0; i < temp.length; i++) {
        temp[i].fill(EMPTY);
    }
    return temp;
}
function getPlayer() {
    return playerCounter % 2 === 0 ? CROSS : ZERO;
}
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
function artificialIntelligenceTurn() {
    let row = 0;
    let col = 0;
    while (true) {
        row = getRandomInt(board.length);
        col = getRandomInt(board.length);
        if (board[row][col] === EMPTY) {
            break;
        }
    }
    clickOnCell(row, col);
}
function highlightWinner(wincondition) {
    let winner = getPlayer();
    switch (wincondition.condition) {
        case 'row':
            for (let c = 0; c < board.length; c++) {
                renderSymbolInCell(winner, wincondition.row, c, 'red');
            }
            break;
        case 'column':
            for (let r = 0; r < board.length; r++) {
                renderSymbolInCell(winner, r, wincondition.col, 'red');
            }
            break;
        case 'main-diag':
            for (let i = 0; i < board.length; i++) {
                renderSymbolInCell(winner, i, i, 'red');
            }
            break;
        case 'sub-diag':
            let i = board.length - 1;
            let j = 0;
            while (i >= 0) {
                renderSymbolInCell(winner, i, j, 'red');
                i--;
                j++;
            }
            break;
    }
}
function checkGameOver() {
    let condition =
        checkRowCondition() ||
        checkColumnCondition() ||
        checkMainDiagCondition() ||
        checkSubDiagCondition();
    if (condition !== false) {
        return condition;
    }
    return false;
}
function isDraw() {
    return playerCounter < 9 ? false : true;
}
function checkRowCondition() {
    let i = 0;
    let j = 0;
    for (i; i < board.length; i++) {
        let currentSymbol = board[i][j];
        for (j = 0; j < board.length; j++) {
            if (board[i][j] !== currentSymbol || board[i][j] === EMPTY) {
                break;
            }
            if (j === board.length - 1) {
                return { condition: 'row', row: i };
            }
        }
    }
    return false;
}
function checkColumnCondition() {
    let j = 0;
    let i = 0;
    for (j = 0; j < board.length; j++) {
        let currentSymbol = board[i][j];
        for (i = 0; i < board.length; i++) {
            if (board[i][j] !== currentSymbol || board[i][j] === EMPTY) {
                break;
            }
            if (i === board.length - 1) {
                return { condition: 'column', col: j };
            }
        }
    }
    return false;
}
function checkMainDiagCondition() {
    let currentSymbol = board[0][0];

    let i = 1;
    while (i < board.length) {
        if (board[i][i] !== currentSymbol || board[i][i] === EMPTY) {
            return false;
        }
        i++;
    }

    return { condition: 'main-diag' };
}
function checkSubDiagCondition() {
    let i = board.length - 1;
    let j = 0;

    let currentSymbol = board[i][j];
    while (i >= 0) {
        if (board[i][j] !== currentSymbol || board[i][j] === EMPTY) {
            return false;
        }
        i--;
        j++;
    }
    return { condition: 'sub-diag' };
}
function makeMove(player, row, col) {
    renderSymbolInCell(player, row, col);
    board[row][col] = player;
}
function displayWinner(player) {
    showMessage(`Победили ${player}`);
}
function displayDraw() {
    showMessage('Победила дружба!');
}
/* обработчик нажатия на клетку */
function cellClickHandler(row, col) {
    // Пиши код тут
    if (isGameOver) {
        showMessage('Игра окончена, не надо тыкать!');
    } else if (board[row][col] === EMPTY) {
        if (getPlayer() === CROSS) {
            makeMove(CROSS, row, col);
        } else {
            makeMove(ZERO, row, col);
        }
    }
    console.log(`Clicked on cell: ${row}, ${col}`);

    let currentResult = checkGameOver();
    if (currentResult) {
        highlightWinner(currentResult);
        displayWinner(getPlayer());
        isGameOver = true;
    }
    if (!isGameOver) {
        playerCounter++;
        if (isDraw()) {
            displayDraw();
            isGameOver = true;
        } else if (getPlayer() === ZERO && !isGameOver) {
            artificialIntelligenceTurn();
        }
    }
}
/* Пользоваться методом для размещения символа в клетке так:
      renderSymbolInCell(ZERO, row, col);
   */

/* обработчик нажатия на кнопку "Сначала" */
function resetClickHandler() {
    console.log('reset!');
    startGame();
}

/* Служебные фукнции для взаимодействия с DOM. Данные функции нельзя редактировать! */
/* Показать сообщение */
function showMessage(text) {
    var msg = document.querySelector('.message');
    msg.innerText = text;
}

/* Нарисовать игровое поле заданного размера */
function renderGrid(dimension) {
    var container = getContainer();
    container.innerHTML = '';

    for (let i = 0; i < dimension; i++) {
        var row = document.createElement('tr');
        for (let j = 0; j < dimension; j++) {
            var cell = document.createElement('td');
            cell.textContent = EMPTY;
            cell.addEventListener('click', () => cellClickHandler(i, j));
            row.appendChild(cell);
        }
        container.appendChild(row);
    }
}

/* Нарисовать символ symbol в ячейку(row, col) с цветом color */
function renderSymbolInCell(symbol, row, col, color = '#333') {
    var targetCell = findCell(row, col);

    targetCell.textContent = symbol;
    targetCell.style.color = color;
}

function findCell(row, col) {
    var container = getContainer();
    var targetRow = container.querySelectorAll('tr')[row];
    return targetRow.querySelectorAll('td')[col];
}

function getContainer() {
    return document.getElementById('fieldWrapper');
}

/* Test Function */
/* Победа первого игрока */
function testWin() {
    clickOnCell(0, 2);
    clickOnCell(0, 0);
    clickOnCell(2, 0);
    clickOnCell(1, 1);
    clickOnCell(2, 2);
    clickOnCell(1, 2);
    clickOnCell(2, 1);
}

/* Ничья */
function testDraw() {
    clickOnCell(2, 0);
    clickOnCell(1, 0);
    clickOnCell(1, 1);
    clickOnCell(0, 0);
    clickOnCell(1, 2);
    clickOnCell(1, 2);
    clickOnCell(0, 2);
    clickOnCell(0, 1);
    clickOnCell(2, 1);
    clickOnCell(2, 2);
}

function clickOnCell(row, col) {
    findCell(row, col).click();
}
